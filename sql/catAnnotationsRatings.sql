drop table if exists depictsEditsViaCAT;
drop table if exists wikidata;
drop table if exists reason;

create table depictsEditsViaCAT (
    commonsPageId int not null,
    commonsPageTitle varchar(255) not null,
    wikidataId varchar(255) not null,
    otherDepicts text not null,
    rating tinyint(1) default null,
    reasonId int default null,
    primary key (commonsPageId, wikidataId)
) engine innodb;

create table wikidata (
    qid varchar(255) not null,
    language varchar(10) not null default 'en',
    label varchar(255),
    primary key (qid)
) engine innodb;

create table reason (
    id int not null auto_increment,
    rating tinyint(1) not null,
    reason varchar(255),
    primary key (id)
) engine innodb;

insert into reason values (null, -1, 'Depicts annotation is not present in image');
insert into reason values (null, -1, 'Depicts annotation used in the wrong sense (e.g. the mathematical concept "slope" for an image of a hill)');
insert into reason values (null, -1, 'Depicts annotation is present in image, but only as an incidental part (e.g. "road surface" for an image of a car)');
insert into reason values (null, -1, 'Depicts annotation is a part of a pre-existing annotation (e.g. "tire" when we already have "car")');
insert into reason values (null, -1, 'Depicts annotation is abstract or invisible (e.g. "happiness" or "electricity" or "visual arts")');
insert into reason values (null, -1, 'Depicts annotation is too general to be useful (e.g. "automotive design" for an image of a car, or "blue" for an image of the sky)');
insert into reason values (null, -1, 'Depicts annotation is general, when we already have a more specific annotation (e.g. "plant" when we already have "oak")');
insert into reason values (null, -1, 'Only part of the item described in the depicts annotation is visible (e.g. "airplane" when only a wing is visible)');
insert into reason values (null, -1, 'Image is a scan of a document, and so probably should not have a "depicts" annotation.');
insert into reason values (null, -1, 'Some other reason');
insert into reason values (null, 0, 'Depicts annotation is general when we already have a more specific annotation, but might be useful anyway (e.g. "dog" when we already have "poodle")');
insert into reason values (null, 0, 'Depicts annotation is more general than we would like, but might be useful anyway (e.g. image of a house annotated with "building" when there are no other annotations)');
insert into reason values (null, 0, 'Depicts annotation only describes one aspect of the image, but might be useful anyway (e.g. image of a cemetery annotated with "tombstone" when there are no other annotations)');
insert into reason values (null, 0, 'Some other reason');