<?php

$config = parse_ini_file( __DIR__ . '/../config.ini', true );
if ( file_exists( __DIR__ . '/../replica.my.cnf' ) ) {
    $config = array_merge(
        $config,
        parse_ini_file( __DIR__ . '/../replica.my.cnf', true )
    );
}
$mysqli = new mysqli( $config['db']['host'], $config['client']['user'],
    $config['client']['password'], $config['db']['dbname'] );
if ( $mysqli->connect_error ) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
        . $mysqli->connect_error);
}

if ( !isset( $_POST['commonsPageId'] ) || !isset( $_POST['depictsForRating'] ) || !isset( $_POST['rating'] ) || !isset( $_POST['reasonId'] ) ) {
    throw new Exception( 'Missing data' );
}

echo "rated depicts " .  $_POST['depictsForRating'] . " for commons page " . intval( $_POST['commonsPageId'] ) .
    " with " . intval( $_POST['rating'] ) . " (reasonId " . $_POST['reasonId'] . "\n";
$mysqli->query(
    'update depictsEditsViaCAT 
    set rating='. intval( $_POST['rating'] ) .',
    reasonId='. intval( $_POST['reasonId'] ) .'
    where commonsPageId=' . intval( $_POST['commonsPageId'] ) . '
    and wikidataId="' . $mysqli->real_escape_string( $_POST['depictsForRating'] ). '"'
);

$mysqli->close();
