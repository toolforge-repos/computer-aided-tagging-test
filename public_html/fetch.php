<?php

function getFileUrl( $filename ) {
    $hash = md5( $filename );
    return 'https://upload.wikimedia.org/wikipedia/commons/' .
        substr( $hash, 0, 1 ) . '/' .
        substr( $hash, 0, 2 ) . '/' .
        $filename;
}

function getCommonsUrl( $filename ) {
    return 'https://commons.wikimedia.org/wiki/File:' . $filename;
}

$config = parse_ini_file( __DIR__ . '/../config.ini', true );
if ( file_exists( __DIR__ . '/../replica.my.cnf' ) ) {
    $config = array_merge(
        $config,
        parse_ini_file( __DIR__ . '/../replica.my.cnf', true )
    );
}
$pdo = new PDO( 'mysql:host=' . $config['db']['host'] . ';dbname=' . $config['db']['dbname'],
    $config['client']['user'], $config['client']['password'] );
if ( $pdo->errorCode() ) {
    die('Connect Error (' . $pdo->errorCode() . ') '
        . $pdo->errorInfo() );
}

if ( $_GET[ 'id' ] ?? false ) {
    $query = 'select *
	from depictsEditsViaCAT 
	where commonsPageId=' . intval( $_GET['id'] );
} else {
    $query = 'select *
	from depictsEditsViaCAT 
	where rating is null 
	order by rand() limit 1';
}

$result = $pdo->query( $query )->fetch( PDO::FETCH_ASSOC );

$qids = array_merge( [ $result['wikidataId'] ], explode( '|', $result['otherDepicts'] ) );
$labelsQuery = 'select qid, label from wikidata where qid in ("' . implode('","', $qids) . '")';
$qidsWithLabels = $pdo->query( $labelsQuery )->fetchAll( PDO::FETCH_KEY_PAIR );

$reasons = $pdo->query("select * from reason")->fetchAll( PDO::FETCH_ASSOC );

$response = [
    'commonsPageId' => $result['commonsPageId'],
    'fileUrl' => getFileUrl( $result['commonsPageTitle'] ),
    'commonsPage' => getCommonsUrl( $result['commonsPageTitle'] ),
    'depictsForRating' => [
        'id' => $result['wikidataId'],
        'label' => $qidsWithLabels[$result['wikidataId']]
    ],
    'otherDepicts' => []
];

unset( $qidsWithLabels[$result['wikidataId']] );
foreach( $qidsWithLabels as $qid => $label) {
    $response['otherDepicts'][] = [ 'id' => $qid, 'label' => $label ];
}

foreach ($reasons as $reason ) {
    $response['reasons'][$reason['rating']][] = [ 'id' => $reason['id'], 'reason' => $reason['reason'] ];
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode( $response, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES );
