<?php

namespace CATAnnotationsTest\Jobs;

require_once 'GenericJob.php';

class PopulateDataJob extends GenericJob {

    public $inputDataPath = __DIR__ . "/../input/T339902_sample.csv";

    public function __construct(array $config = null)
    {
        parent::__construct($config);
        $this->setLogFileHandle( __DIR__ . '/../' . $this->config['log']['jobs'] );
    }

    public function run() {
        $this->log( "Starting data population job" );
        if ( ($handle = fopen( $this->inputDataPath, "r") ) !== false ) {
            while ( ($row = fgetcsv($handle, 1000, ",") ) !== false ) {
                $pageId = $row[1];
                $mediaInfoId = 'M' . $pageId;
                $pageTitle = $row[2];
                $commentText = $row[3];

                $revisionQid = $this->getEditQID( $commentText );
                $otherQids = array_diff( $this->getDepictedItemsQids( $mediaInfoId ), [ $revisionQid ] );

                $this->saveEditData( $pageId, $pageTitle, $revisionQid, $otherQids );
                $this->saveLabels( array_merge( $otherQids, [ $revisionQid ] ) );
            }
            fclose( $handle );
        } else {
            $this->log( "Can't open input file" );
        }
        $this->log( "Finished population job" );
    }

    public function getEditQID( string $commentText ): string {
        if ( !preg_match( '/P180.*(Q[0-9]*)/', $commentText, $matches ) ) {
            $this->log( "Can't find QID in " . $commentText );
        }
        return $matches[1];
    }

    public function getDepictedItemsQids( string $mediaInfoId ): array {
        $qids = [];
        $mediaInfoResponse = $this->httpGETJson( $this->config['endpoint']['getMediaInfoForPageId'], $mediaInfoId );
        $statements = $mediaInfoResponse['entities'][$mediaInfoId]['statements'] ?? ["P180" => []];
        foreach ( $statements["P180"] as $depictsSnak ) {
            $qids[] = $depictsSnak["mainsnak"]["datavalue"]["value"]["id"];
        }
        return $qids;
    }

    public function saveEditData( int $pageId, string $pageTitle, string $revisionQid, array $otherQids ): void {
        $query = 'INSERT IGNORE INTO depictsEditsViaCAT SET ' .
            'commonsPageId = ' . intval( $pageId ) . ', ' .
            'commonsPageTitle="' . $this->dbEscape( str_replace('""', '"', $pageTitle ) ) . '", ' .
            'wikidataId="' . $this->dbEscape( $revisionQid ) . '", ' .
            'otherDepicts="' . $this->dbEscape( implode( '|', $otherQids ) ) . '"';
        $this->db->query( $query );
    }

    public function saveLabels( array $qids ): void {
        $result = $this->httpGETJson( $this->config['endpoint']['getLabelsForQids'], implode( '|', $qids ) );
        if ( !is_array( $result["entities"] ) ) {
            return;
        }
        foreach ( $result["entities"] as $qid => $data ) {
            $query = 'INSERT IGNORE INTO wikidata SET ' .
                'qid="' . $this->dbEscape( $qid ). '",' .
                'label="' . $this->dbEscape( $data['labels']['en']['value'] ) . '" ';
            $this->db->query( $query );
        }
    }
}

$job = new PopulateDataJob();
$job->run();